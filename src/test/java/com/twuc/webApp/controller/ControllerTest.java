package com.twuc.webApp.controller;

import ch.qos.logback.core.util.ContentTypeUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
@AutoConfigureMockMvc
class ControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_200_when_bind_to_int() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/bind/int/15"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("Successful"));
    }


    @Test
    void should_return_200_when_bind_to_integer() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/bind/15"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("15"));
    }


    @Test
    void should_return_200_when_bind_to_int_and_integer() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/15/books/15 "))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("Successful"));
    }

    @Test
    void should_return_200_when_bind_query_string() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users?username=xiaoming"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("xiaoming"));
    }

    @Test
    void should_return_200_and_query_when_bind_query_string() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/getuser?username=xiaoming"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("xiaoming"));
    }

    @Test
    void should_return_200_and_query_user_deafult_query_string() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/getuserbydeafult"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("xiaoming"));
    }

    @Test
    void should_bind_collection_to_param() throws Exception {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("myValue1");
        arrayList.add("myValue2");
        arrayList.add("myValue3");
        mockMvc.perform(MockMvcRequestBuilders.get("/api/bind/collection?myparam=myValue1&myparam=myValue2&myparam=myValue3"))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void should_return_date_when_query_date_string() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/datetimes").content("{ \"dateTime\": \"2019-10-01T10:00:00Z\" }")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("\"2019-10-01\""));
    }


    @Test
    void should_return_date_when_query_incorrect_date_string() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/datetimes").content("{ \"dateTime\": \"2019-10-01\" }")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("\"2019-10-01\""));
    }
}