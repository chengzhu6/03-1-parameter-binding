package com.twuc.webApp.entity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;

public class JsonTest {

    @Test
    void should_convert_object_to_json() throws JsonProcessingException {
        Contract contract = new Contract("aaa", "1110");
        ObjectMapper objectMapper = new ObjectMapper();
        String s = objectMapper.writeValueAsString(contract);
        System.out.println(s);
    }


    @Test
    void should_convert_json_to_object() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        Contract contract = objectMapper.readValue("{\"userName\":\"xxxx\",\"phone\":\"110\"}", Contract.class);
        System.out.println(contract);
    }
}
